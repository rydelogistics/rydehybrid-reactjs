import React, {Component } from 'react'
import {StatusBar,ScrollView,Image,ImageBackground,Alert,View, Text, TextInput, Button, TouchableOpacity ,StyleSheet } from 'react-native'

class SignUpScreen extends Component{
    state ={fullName: "", email: "", password: "", currentPassword: "", phoneNumber: ""}
    constructor(props){
        super(props);
        this.state ={
            fullName:"",
            email:"",
            password:"",
            currentPassword:"",
            phoneNumber:""
        }
    }
    static navigationOptions = {
        header: null
    }

    onSignUp(){
        let details = {
            'name': this.state.fullName,
            'email': this.state.email,
            'phone': this.state.phoneNumber,
            'password': this.state.password
        };
    
        let formBody = [];
        for (let property in details) {
            let encodedKey = encodeURIComponent(property);
            let encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
    
        fetch('url', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formBody
        }).then((response) => response.json())
            .then((responseData) => {
                console.log(responseData);
    
    
                AlertIOS.alert(
                    "POST Response",
                    "Response Body " + JSON.stringify(responseData.role)
                );
            })
            .done();
        // const {fullName,email,password, currentPassword,phoneNumber} = this.state
        // if(fullName==""){
        //     Alert.alert('please fill the first name')
        // }
        // else if(email==""){
        //     Alert.alert('please enter your email name')
        // }
        // else if(password==""){
        //     Alert.alert('please enter your password')
        // }
        // // else if(currentPassword==""){
        //     // Alert.alert('please confirm password')
        // // }
        // else if(password !== currentPassword){
        //     Alert.alert('Password and  confirm password must be the same')
        // }
        // else if(phoneNumber==""){
        //     Alert.alert('please enter your phone number')
        // }
        // else{
        //     Alert.alert('Sign Up Successful')
        //     this.props.navigation.navigate('home')
        // }
    }
    
    navigateToLogin(){
        this.props.navigation.navigate('Login')
    }

    render(){
     
        return(
            <ImageBackground source={require('../Assets/login-background.jpg')} style= {styles.container}>
            <ScrollView style={styles.contentContainer}>
            <StatusBar
                backgroundColor="#1c313a"
                barStyle="light-content"
            />
            <View style = {styles.imageContainer}>
             <Image style={{width:60, height:60}} source ={require('../Assets/logo.jpg')}/> 
             <Text style ={styles.logoText}>Welcome to Ryde Logistics</Text>
            </View>
            <View style= {styles.inputContainer}>
            <TextInput style = {styles.inputBox}
            underlineColorAndroid='rgba(0,0,0,0)'
            placeholderTextColor="#000"
            placeholder ="Enter your fullname"
            onChangeText ={text=> this.setState({fullname:text})}/>
            <TextInput style = {styles.inputBox} 
            underlineColorAndroid ='rgba(0,0,0,0)'
            placeholderTextColor="#000" 
            placeholder = "Enter your Email"
            keyboardType='email-address' 
            onChangeText={text=> this.setState({email: text})}/>
            <TextInput style = {styles.inputBox} 
            underlineColorAndroid ='rgba(0,0,0,0)'
            placeholderTextColor="#000" 
            placeholder = "Password" secureTextEntry ={true}
            onChangeText={text => this.setState({password: text})}/>
            <TextInput style = {styles.inputBox} 
            underlineColorAndroid ='rgba(0,0,0,0)' 
            placeholderTextColor= "#000"
            placeholder = "Confirm password" secureTextEntry ={true}
            onChangeText={text => this.setState({currentPassword: text})}/>
            <TextInput style ={styles.inputBox}
            underlineColorAndroid='rgba(0,0,0,0)'
            placeholderTextColor="#000"
            placeholder ="Phone Number"
            onChangeText={text => this.setState({phoneNumber: text})}/>
            <TouchableOpacity style ={styles.button} onPress ={ _ =>this.onSignUp()}><Text style ={styles.buttonText}>SignUp</Text></TouchableOpacity>
            </View>
            <View style= {styles.signUpTextCont}>
            <Text style = {styles.signUpButton}> Already have an account?</Text>
            <TouchableOpacity onPress ={_ =>this.navigateToLogin()}>
            <Text style = {styles.signUpButton}> Login</Text>
            </TouchableOpacity>
            </View>
            </ScrollView>
            </ImageBackground>
          

        
        )
           
    }
}

export default SignUpScreen;

const styles = StyleSheet.create({
    container:{
        // backgroundColor: '#455a64',
        flex:1,
        alignItems: 'center',
        justifyContent:'center'
    },

    textColor:{
        color: '#ffffff',
        fontSize: 18
    },
    
    imageContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems:'center'
    },

    logoText:{
        fontWeight:'500',
        marginVertical:15,
        fontSize:18,
        color: 'rgba(255, 255, 255, 0.7)'
    },

    inputBox:{
        width: 300,
        backgroundColor:'rgba(255,255,255,0.4)',
        borderRadius: 25,
        paddingHorizontal: 16,
        paddingVertical: 10,
        fontSize: 16,
        marginVertical: 10,
        color:'#ffffff'
    },
    inputContainer:{
        flex:1,
        justifyContent:'flex-end',
        alignItems: 'center',
        marginVertical: 16
    },
    buttonText:{
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    button: {
        backgroundColor:'#ed8323',
        borderRadius: 25,
        marginVertical: 10,
        width: 300,
        paddingVertical:12,
        marginVertical: 10

    },
    signUpTextCont:{
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent: 'center',
        flexDirection: 'row',
        marginVertical:16
    },
    signUpButton :{
        color: '#ffffff',
        fontSize:16,
        fontWeight:'500'
    },
    contentContainer: {
        paddingVertical: 30
      }

})