import React, {Component} from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity, Image, Alert, StatusBar, TextInput} from 'react-native';

class LoginScreen extends Component {
    state = {email: "", password: " "}

    static navigationOptions = {
        header: null
    }

signUpCredentials(){
    this.props.navigation.navigate('signUp')
}
checkLogin(){

    let details = {
        email: 'devwilfredagyei@gmail.com',
        password: 'wilfred'
    };

    let formBody = [];
    for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch('https://ryde-rest.herokuapp.com/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
    }).then((response) => response.json())
        .then((responseData) => {

            this.props.navigation.navigate('Home')
            // console.log(responseData);


            // Alert.alert(
                // "POST Response",
                // "Response Body " + JSON.stringify(responseData.role)
            // );
        })
        .done();
    
      
     }

    render(){
        return(
                <ImageBackground source={require('../Assets/login-background.jpg')} style= {styles.container}>
                <StatusBar
                    backgroundColor="#ed8323"
                    barStyle="light-content"
                />
                <View style = {styles.imageContainer}>
                 <Image style={{width:60, height:60}} source ={require('../Assets/logo.jpg')}/> 
                 <Text style ={styles.logoText}>Welcome to Ryde Logistics</Text>
                </View>
                <View style= {styles.inputContainer}>
                <TextInput style = {styles.inputBox} 
                underlineColorAndroid ='rgba(0,0,0,0)' 
                placeholder = "Enter your Email"
                placeholderTextColor="#000"
                keyboardType='email-address' 
                onChangeText={text=> this.setState({email: text})}/>
                <TextInput style = {styles.inputBox} 
                underlineColorAndroid ='rgba(0,0,0,0)' 
                placeholder = "Password" secureTextEntry ={true}
                placeholderTextColor="#000"
                onChangeText={text => this.setState({password: text})}/>
                <TouchableOpacity style ={styles.button} onPress ={ _ =>this.checkLogin()}><Text style ={styles.buttonText}>Login</Text></TouchableOpacity>
                </View> 
                <View style= {styles.signUpTextCont}>
                <TouchableOpacity onPress ={ _ =>this.signUpCredentials()}>
                <Text style = {styles.signUpButton}> Create account |</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                <Text style = {styles.signUpButton}> Forgot password</Text>
                </TouchableOpacity>
                </View>
                </ImageBackground>
        )
    }
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent:'center',
    },

    textColor:{
        color: '#ffffff',
        fontSize: 18
    },
    
    imageContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems:'center'
    },

    logoText:{
        fontWeight: '500',
        marginVertical:15,
        fontSize:18,
        color: 'rgba(255, 255, 255, 0.7)'
    },

    inputBox:{
        width: 300,
        backgroundColor:'rgba(255,255,255,0.4)',
        borderRadius: 25,
        paddingHorizontal: 16,
        paddingVertical: 10,
        fontSize: 16,
        marginVertical: 10,
        color:'#000000'
    },
    inputContainer:{
        flex:1,
        justifyContent:'flex-end',
        alignItems: 'center',
        marginVertical: 16
    },
    buttonText:{
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    button: {
        backgroundColor:'#ed8323',
        borderRadius: 25,
        marginVertical: 10,
        width: 300,
        paddingVertical:12,
        marginVertical: 10

    },
    signUpTextCont:{
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent: 'center',
        flexDirection: 'row',
        marginVertical:16
    },
    signUpButton :{
        color: '#ffffff',
        fontSize:16,
        fontWeight:'500'
    }

  });