import React, {Component} from 'react';
import { View, Text, StyleSheet} from 'react-native';
import {createBottomTabNavigator, DrawerNavigator} from 'react-navigation';

import RydeScreen from './RydeScreen';
import NotificationScreen from './NotificationScreen';
import InfoScreen from './InfoScreen';
import Icon from 'react-native-vector-icons/Ionicons';

class HomeScreen extends Component {
    render(){
        return(
            <View style ={StyleSheet.container}>
                <Text>HomeScreen</Text>
            </View>
        )
    }
}

export default createBottomTabNavigator({ 
    homeScreen:{
        screen: HomeScreen,
        navigationOptions:{
            tabBarLabel: 'Home',
            tabBarIcon: ({tintColor}) =>(
                <Icon name="ios-home" color={tintColor} size={25}/>
            )
        }, 
    },
    
    ryde: {
        screen: RydeScreen,
        navigationOptions:{
          tabBarLabel: 'Ryde',
          tabBarIcon: ({tintColor})=>(
              <Icon name ="ios-car" color={tintColor} size={25}/>
          )  
        }
    },
    notifications: {
        screen: NotificationScreen,
        navigationOptions:{
          tabBarLabel: 'Notifications',
          tabBarIcon: ({tintColor})=>(
              <Icon name ="ios-notifications" color={tintColor} size={25}/>
          )  
        }
    },
    help: {
        screen: InfoScreen,
        navigationOptions:{
          tabBarLabel: 'Info',
          tabBarIcon: ({tintColor})=>(
              <Icon name ="ios-information-circle" color={tintColor} size={25}/>
          )  
        }
    },
    
},{
    navigationOptions: {
        tabBarVisible: true
    },
    tabBarOptions:{
        activeTintColor: '#ed8323',
        inactiveTintColor: 'grey'
    }
})