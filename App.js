import React from 'react';
import {createStackNavigator} from 'react-navigation';

import LoginScreen from './components/screens/LoginScreen'
import HomeScreen from './components/screens/HomeScreen'
import SignUpScreen from './components/screens/SignUpScreen';
export default class App extends React.Component {
  render() {
    return (
      <AppStackNavigator />
    );
  }
}

const AppStackNavigator=createStackNavigator({
  Login: LoginScreen,
  signUp: SignUpScreen,
  Home: HomeScreen
})
